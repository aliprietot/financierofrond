import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AuthService } from "../../settings/services/auth.service";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../../settings/services/app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [DatePipe],
})
export class LoginComponent implements OnInit {

  public datos: FormGroup;
  public fecha = '';

  constructor( 
    public formBuilder: FormBuilder,
    private router: Router,
    private appService: AppService,
    private auth: AuthService,
    private _DatePipe:DatePipe ) { 
    const date = new Date();
    this.fecha = this._DatePipe.transform(date, 'yyy-MM-dd');
    this.datos = this.formBuilder.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  async ngOnInit() {
   
  }


  public login() {
    if (this.datos.valid) {
      const datos = this.datos.value;
      let user: any;
      this.appService.openSpinner();
      this.auth.login({ username: datos.usuario, password: datos.password}).subscribe(
        (data: any) => {
          console.log(data);
          this.appService.closeSpinner();
          this.appService.showSuccess('INGRESADO');
          this.auth.setToken(data.access_token);
          this.router.navigate(['modulos']);
        },
        error => {
          console.log(error);
          this.appService.closeSpinner();
          this.appService.showError('Error al Ingresar');
        }
      );
    }
  }

  public onKeydown(event) {
    if (event.key === "Enter") {
      this.login();
    }
  }

  public GO() {
    window.open('http://portal.tributo.co/');
  }
}
