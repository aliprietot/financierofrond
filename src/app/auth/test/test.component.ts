import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AlertService } from '../../settings/services/alert.service';
import { AppService } from '../../settings/services/app.service';
import { menu_principal } from '../../settings/const';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  providers: [DatePipe],
})
export class TestComponent implements OnInit {

  public menu: Array<any>;

  constructor( 
    private _AlertService: AlertService,
    private _ApiRestService: AppService ) { 
      this.menu = menu_principal;
  }

  async ngOnInit() {
    this._AlertService._Destroy();
  }

  public goTo(ruta: string, estado: boolean) {
    if (estado === true) {
      this._ApiRestService.goTo(ruta);
    } else {
      if (estado === false) {
        this._AlertService.showWarning('No se puede acceder!');
      }
    }
  }
}
