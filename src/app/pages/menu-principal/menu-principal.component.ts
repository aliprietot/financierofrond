import { Component, OnInit, ViewChild } from '@angular/core';

import { AuthService } from '../../settings/services/auth.service';
import { menu_principal } from '../../settings/const';
import { NbIconLibraries } from '@nebular/theme';
import { AlertService } from '../../settings/services/alert.service';
import {AppService} from '../../settings/services/app.service'


@Component({
  selector: 'app-menu-principal',
  templateUrl: './menu-principal.component.html',
  styleUrls: ['./menu-principal.component.scss'],
})

export class MenuPrincipalComponent implements OnInit {

  public menu: Array<any>;
  public pageNow = 1;
  public on = true;
  public evaIcons = [];

  constructor(
    private _AlertService: AlertService,
    private iconsLibrary: NbIconLibraries,
    private _AuthService: AuthService,
    private _ApiRestService: AppService) {
    this.menu = menu_principal;
    this.evaIcons = Array.from(iconsLibrary.getPack('eva').icons.keys())
    .filter(icon => icon.indexOf('outline') === -1);
    iconsLibrary.registerFontPack('fa', { packClass: 'fa', iconClassPrefix: 'fa' });
    iconsLibrary.registerFontPack('far', { packClass: 'far', iconClassPrefix: 'fa' });
   }

  ngOnInit() {
  }

  public goTo(ruta: string, estado: boolean) {
    if (estado === true) {
      this._ApiRestService.goTo(ruta);
    } else {
      if (estado === false) {
        this._AlertService.showWarning('No se puede acceder!');
      }
    }
  }

}
