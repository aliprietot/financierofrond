import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { MenuPrincipalComponent } from './menu-principal/menu-principal.component';
import { SharedModule } from '../settings/shared/shared.module';
import { MenuContabilidadComponent } from './menu-contabilidad/menu-contabilidad.component';
import {MenuPresupuestoComponent} from './menu-presupuesto/menu-presupuesto.component';


@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    SharedModule,
  ],
  declarations: [
    PagesComponent,
    MenuPrincipalComponent,
    MenuContabilidadComponent,
    MenuPresupuestoComponent,
  ],
})
export class PagesModule {
}
