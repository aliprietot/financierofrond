import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from '../pages/pages.component';
import { MenuPrincipalComponent } from './menu-principal/menu-principal.component';
import { MenuContabilidadComponent } from './menu-contabilidad/menu-contabilidad.component';
import { MenuPresupuestoComponent } from './menu-presupuesto/menu-presupuesto.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'presupuesto',
      loadChildren: () => import('./presupuesto/presupuesto.module').then(m => m.PresupuestoModule),
    },
    { path: 'menu-principal', component: MenuPrincipalComponent },
    { path: 'menu-contabilidad', component: MenuContabilidadComponent },
    { path: 'menu-presupuesto', component: MenuPresupuestoComponent },
    {
      path: '',
      redirectTo: 'menu-principal',
      pathMatch: 'full',
    },
    {
      path: '**',
      redirectTo: 'menu-principal',
    },
    /* ,
  {
    path: 'contabilidad',
    loadChildren: () => import('./').then(m => m.PresupuestoModule),
    data: { breadcrumb: 'presupuesto'},
  }  */

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule { }

