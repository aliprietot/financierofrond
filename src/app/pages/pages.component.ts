import { Component, ViewChild } from '@angular/core';
import { MENU_ITEMS, MENE_USER } from './pages-menu';
import { AuthService } from '../settings/services/auth.service';
import { AppSettings } from '../settings/app.settings';
import { Settings } from '../settings/app.settings.model';
import { AppService } from '../settings/services/app.service';
import {} from '../settings/const'

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  templateUrl: './pages.component.html',
})

export class PagesComponent {

  public usuario: any;
  public menu: any;
  public settings: Settings;

  constructor(
    public appSettings: AppSettings,
    private _ApiRestService: AppService,
    private _AuthService: AuthService,
  ) {
    this.settings = this.appSettings.settings;
    this.usuario = this._AuthService.getUsuarioLogueado(sessionStorage.getItem('token'));
    console.log(this.usuario);
  }
}
