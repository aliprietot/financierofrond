import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../settings/services/app.service';
import { AuthService } from '../../settings/services/auth.service';
import { NbIconLibraries } from '@nebular/theme';
import { AlertService } from '../../settings/services/alert.service';
import { AppSettings } from '../../settings/app.settings';
import { Settings } from '../../settings/app.settings.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-menu-contabilidad',
  templateUrl: './menu-contabilidad.component.html',
  styleUrls: ['./menu-contabilidad.component.scss'],
})

export class MenuContabilidadComponent implements OnInit {

  public menu: Array<any>;
  public pageNow = 1;
  public on = true;
  public settings: Settings;
  public usuario: any;
  public evaIcons = [];

  constructor(
    private _Location: Location,
    public appSettings: AppSettings,
    private _AlertService: AlertService,
    iconsLibrary: NbIconLibraries,
    private _AuthService: AuthService,
    private _ApiRestService: AppService) {
    this.evaIcons = Array.from(iconsLibrary.getPack('eva').icons.keys())
    .filter(icon => icon.indexOf('outline') === -1);
    iconsLibrary.registerFontPack('fa', { packClass: 'fa', iconClassPrefix: 'fa' });
    iconsLibrary.registerFontPack('far', { packClass: 'far', iconClassPrefix: 'fa' });
   }

  ngOnInit() {}

  public goTo(ruta: string, estado: boolean) {
    if (estado === true) {
      this._ApiRestService.goTo(ruta);
    } else {
      if (estado === false) {
        this._AlertService.showWarning('No se puede acceder!');
      }
    }
  }

}
