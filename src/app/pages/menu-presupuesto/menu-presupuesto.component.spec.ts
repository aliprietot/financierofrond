import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuPresupuestoComponent } from './menu-presupuesto.component';

describe('MenuPresupuestoComponent', () => {
    let component: MenuPresupuestoComponent;
    let fixture: ComponentFixture<MenuPresupuestoComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MenuPresupuestoComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MenuPresupuestoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});