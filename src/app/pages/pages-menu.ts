import { NbMenuItem } from '@nebular/theme';

export const MENE_USER: NbMenuItem[] = [
  {
    title: 'ADMINISTRADOR ENTIDAD',
    group: true,
  },
  {
    title: 'Escritorio',
    icon: 'browser-outline',
    link: '/pages',
  },
];

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'ADMINISTRADOR GTS',
    group: true,
  },
  {
    title: 'Escritorio',
    icon: 'browser-outline',
    link: '/pages',
  },
  {
    title: 'Reportes',
    icon: 'file-text-outline',
    link: '/pages/reportes',
  },
  {
    title: 'Configuracion',
    icon: 'settings-outline',
    children: [
      {
        title: 'Perfil Entidad',
        link: '/pages/config/perfil',
      },
      {
        title: 'Parametros Admin',
        link: '/pages/config/parametros-admin',
      },
      {
        title: 'Parametros Generales',
        link: '/pages/config/parametros-generales',
      },
      {
        title: 'Administrar Tramites',
        link: '/pages/config/tramites-admin',
      },
      {
        title: 'Administrar Tributos',
        link: '/pages/config/tributos-admin',
      },
      {
        title: 'Entidades',
        link: '/pages/config/main',
      },
    ],
  },
];
