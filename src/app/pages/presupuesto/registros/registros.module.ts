import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RegistrosComponent} from './registros.component'    
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../settings/shared/shared.module';

export const routes: Routes = [
  {path: '', component: RegistrosComponent, pathMatch: 'full'},
];

@NgModule({
  declarations: [RegistrosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
})
export class RegistrosModule { }
