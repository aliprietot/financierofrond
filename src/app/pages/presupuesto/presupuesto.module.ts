import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../settings/shared/shared.module';
import { PresupuestoComponent } from './presupuesto.component';

import {
  CentroCostosComponent,
  HomologacionComponent,
  ConfiguracionesPresupuestoComponent,
  TercerosComponent,
  TerceroFromcomponent
  //ConfiguracionesComponent
} from  './configuraciones-presupuesto/index';

import {
  CertificadoComponent,
  InterfasesComponent,
  RegistrosComponent
} from './index';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PresupuestoComponent,
  },
  {path: 'configuraciones-presupuesto', component: ConfiguracionesPresupuestoComponent},
  {path: 'configuraciones-presupuesto/homologacion', component: HomologacionComponent},
  {path: 'configuraciones-presupuesto/centro-costo', component: CentroCostosComponent},
  {path: 'interfases', component: InterfasesComponent},
  {path: 'certificado', component: CertificadoComponent},
  {path: 'registro', component: RegistrosComponent},
  {path: 'configuraciones-presupuesto/terceros', component: TercerosComponent},
  //{path: 'configuraciones-presupuesto/configuraciones', component: ConfiguracionesComponent}

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  declarations: [
    PresupuestoComponent,
    CentroCostosComponent,
    HomologacionComponent,
    ConfiguracionesPresupuestoComponent,
    InterfasesComponent,
    RegistrosComponent,
    CertificadoComponent,
    TercerosComponent,
    TerceroFromcomponent
   // ConfiguracionesComponent
  ],
  entryComponents: [
    TerceroFromcomponent
  ]
})

export class PresupuestoModule { }
