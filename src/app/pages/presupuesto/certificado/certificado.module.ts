import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CertificadoComponent} from './certificado.component'    
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../..//settings/shared/shared.module';

export const routes: Routes = [
  {path: '', component: CertificadoComponent, pathMatch: 'full'},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
})
export class CertificadoModule { }
