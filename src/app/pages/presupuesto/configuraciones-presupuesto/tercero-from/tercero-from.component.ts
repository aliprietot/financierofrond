import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material/dialog";


@Component({
    selector: "app-tercero-from",
    templateUrl: "./tercero-from.component.html",
    styleUrls: ["./tercero-from.component.scss"],
})

export class TerceroFromcomponent implements OnInit {

    public show = false;
    public datos: FormGroup;


    constructor(
        public formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<TerceroFromcomponent>
    ) {
        this.datos = this.formBuilder.group({
            vigencia: ['', Validators.required],
            consId: ['', Validators.required],
            consDes: ['', Validators.required],
          });

    }
     ngOnInit() {
     
    }

   
  

    public close(tipo: number): void {
        this.dialogRef.close({ tipo: tipo, data: '' });
    }

 
}
