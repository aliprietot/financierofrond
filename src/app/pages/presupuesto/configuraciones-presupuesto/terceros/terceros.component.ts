import { SelectionModel } from "@angular/cdk/collections";
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TerceroFromcomponent } from '../tercero-from/tercero-from.component';
import {AlertService} from '../../../../settings/services/alert.service';
import {AppService} from '../../../../settings/services/app.service';

@Component({
  selector: 'app-terceros',
  templateUrl: './terceros.component.html',
  styleUrls: ['./terceros.component.scss'],
})
export class TercerosComponent implements OnInit {

  public contribuyente: any;
  public predio: Array<any> = [];
  public evaIcons = [];
  public displayedColumns: string[] = ['terCod', 'terNombre', 'terNombre2', 'terApellido', 'terApellido2', 'terCel', 'terDir', 'terMail', 'estado'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  public dataSource: any;
  public itemSelected: any;
  public selection = new SelectionModel<any>(true, []);



  constructor(
    public dialog: MatDialog,
    private AlertService: AlertService,
    private apiService: AppService
  ) { 
    this.dataSource = new MatTableDataSource<any>([]);
  }

  ngOnInit() {
    this.getTerceros();
  }


  public  async getTerceros(){
    try {
      this.AlertService.changeLoadingText('consultando los terceros');
      const terceros: Array<any> = await this.apiService.get('gterceroptal');
      this.dataSource = new MatTableDataSource<any>(terceros);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.AlertService.showInfo('Listado de Terceros!');
    } catch (error) {
      this.AlertService.showWarning('ERROR EN EL SERVIDOR')
    }
  }

  public nuevo() {
    this.itemSelected = null;
    this.openForm(0);
  }

  public editar(item: any) {
    this.itemSelected = item;
    this.openForm(1);
  }

   public async openForm(tipoForm: number) {
        const dialogRef = this.dialog.open(TerceroFromcomponent, {
          width: '60%', height: 'auto', disableClose: true, backdropClass: 'dark', panelClass: 'panel',
        });
        dialogRef.afterClosed().subscribe(async (result) => {
            if (result === 1) {
                await this.getTerceros();
            }
        });
    }



}