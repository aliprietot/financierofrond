import { Component, OnInit, ViewChild, Inject, Input } from '@angular/core';
import {menu_configuraciones} from '../../../settings/const';
import {AppService} from '../../../settings/services/app.service';
import {AlertService}from  '../../../settings/services/alert.service';
@Component({
  selector: 'app-configuraciones-presupuesto',
  templateUrl: './configuraciones-presupuesto.component.html',
  styleUrls: ['./configuraciones-presupuesto.component.scss'],

})

export class ConfiguracionesPresupuestoComponent implements OnInit {

  public menu: Array<any>;
  

  constructor(
    private _ApiRestService: AppService,
    private _AlertService:AlertService) {
    
    this.menu = menu_configuraciones;
  }
  ngOnInit() {}

  public goTo(ruta: string, estado: boolean) {
    if (estado === true) {
      this._ApiRestService.goTo(ruta);
    } else {
      if (estado === false) {
        this._AlertService.showWarning('No se puede acceder!');
      }
    }
  }


}
