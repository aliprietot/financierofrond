import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InterfasesComponent} from './Interfases.component'    
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../../settings/shared/shared.module';

export const routes: Routes = [
  {path: '', component: InterfasesComponent, pathMatch: 'full'},
];

@NgModule({
  declarations: [InterfasesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
})
export class InterfasesModule { }
