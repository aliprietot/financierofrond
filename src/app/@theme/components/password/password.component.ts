import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../../../settings/services/app.service';
import { AuthService } from '../../../settings/services/auth.service';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { AlertService } from '../../../settings/services/alert.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
})

export class PasswordComponent implements OnInit {

  public datos: FormGroup;
  public show = false;
  public usuario: any;
  public usuarioToSend: any;

  constructor(
    private _AlertService: AlertService,
    private _AuthService: AuthService,
    private _ApiRestService: AppService,
    public dialogRef: MatDialogRef<PasswordComponent>,
    public formBuilder: FormBuilder,
  ) {
    this.datos = this.formBuilder.group({
      actual: ['', Validators.required],
      nueva: ['', Validators.required],
      confirm: ['', RxwebValidators.compare({fieldName: 'nueva'})],
    });
    this.usuario = this._AuthService.getUsuarioLogueado(sessionStorage.getItem('token'));
  }

  ngOnInit() {
    setInterval(() => { this.show = true; }, 1000);
    this.getUsuarioToSend();
  }

  public async getUsuarioToSend () {
    this.usuarioToSend = await this._ApiRestService.get('/users/find/id/' + this.usuario.id);
  }

  public setUser() {
    return {
      usuario: {
        ...this.usuarioToSend,
        password: this.usuarioToSend.password = this.datos.value.actual,
      },
      newpassword: this.datos.value.nueva,
    };
  }

  public updatePassword() {
    this._AlertService.confirmAlert('Seguro Que desea Cambiar la Contraseña?')
    .then((e: any) => {
      if ( e === true ) {
        this._AlertService.changeLoadingText('Actualizando Contraseña');
        this._ApiRestService.put('/user/update/password', this.setUser()).subscribe(
          (data: any) => {
            this.close(0);
            this._AuthService.logout();
          },
        );
      }
    });
  }

  public close(tipo: number): void {
    this.dialogRef.close(tipo);
  }
}
