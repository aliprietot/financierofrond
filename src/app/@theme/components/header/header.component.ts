import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';
import { LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AuthService } from '../../../settings/services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { AppService } from '../../../settings/services/app.service';
import { Location, DatePipe } from '@angular/common';
import { AlertService } from '../../../settings/services/alert.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})

export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  public userPictureOnly: boolean = false;
  public user: any;
  public currentTheme = 'default';
  public userMenu = [{ title: 'Profile' }, { title: 'Log out' }];
  public username = '';
  public usuario: any;
  public imgUrl = 'assets/img/default.png';
  public nombreEntidad = '';
  public fecha = '';

  constructor(
    private _DatePipe: DatePipe,
    private _AlertService: AlertService,
    private _Location: Location,
    private _ApiRestService: AppService,
    public dialog: MatDialog,
    private _AuthService: AuthService,
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private layoutService: LayoutService,
    private http: HttpClient,
    private breakpointService: NbMediaBreakpointsService) {
    this.userMenu = this._AuthService.getUsuarioLogueado(sessionStorage.getItem('token')).user_name;
    this.user = this._AuthService.getUsuarioLogueado(sessionStorage.getItem('token'));
    const date = new Date();
    this.fecha = this._DatePipe.transform(date, 'yyy-MM-dd');
    // this.getDataEmpresa();
  }

  async ngOnInit() {
    await this.getUrlImg();
    this.currentTheme = this.themeService.currentTheme;

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public async getUrlImg() {
    const data: any = await this._ApiRestService.getEndPoints();
    this.imgUrl = String(data.uploads + this.user.entidad.logo);
  }

  public changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  public async getEndPoints(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const url = String(window.location.href).split('#')[0];
      this.http.get(`${url}assets/endpoints/endpoint.json`)
      .subscribe((data: any) => {resolve(data); });
    });
  }

  public toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  public navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  public logOut() {
    this._AlertService.confirmAlert('Seguro que desea cerrar Sesion?')
    .then((e: any) => {if (e === true ) {this._AuthService.logout(); }});
  }
/* 
  public async getDataEmpresa() {
    const idEntidad = this._AuthService.getUsuarioLogueado(sessionStorage.getItem('token')).entidad.idEntidad;
    this._AlertService.changeLoadingText('consultando datos de la entidad!');
    //const entidad = await this._ApiRestService.get(`/entidad/${idEntidad}`);
    // console.log(entidad);
    this.nombreEntidad = 'TUBARA';
  }
 */
  public goBack() {
    this._Location.back();
  }
}
