import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GdiviposComponent } from './gdivipos.component';

describe('GdiviposComponent', () => {
    let component: GdiviposComponent;
    let fixture: ComponentFixture<GdiviposComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GdiviposComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GdiviposComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});