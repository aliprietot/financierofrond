import { Component, OnInit, ViewChild } from '@angular/core';
import { divipos } from '../../../settings/imports/geDivipo.data';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-gdivipos',
  templateUrl: './gdivipos.component.html',
  styleUrls: ['./gdivipos.component.scss'],
})
export class GdiviposComponent implements OnInit {

  public datos: FormGroup;
  public departamentos: Array<any> = [];
  public ubicaciones: Array<any> = [];
  public departamentoSelected: any;

  public displayedColumns = [ 'select', 'divipo', 'codigo', 'nombre'];
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  public dataSource: any;
  public itemSelected: any;
  public selection = new SelectionModel<any>(true, []);
  public selected = false;

  constructor(
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GdiviposComponent>) {
    this.ubicaciones = divipos;
    this.dataSource = new MatTableDataSource<any>([]);
    this.datos = this.formBuilder.group({
      divipo: ['', Validators.required],
      codigo: ['', Validators.required],
      nombre: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.getDepartamentos();
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) { this.dataSource.paginator.firstPage(); }
  }

  public getDepartamentos() {
    let last_departament: any = null;
    this.ubicaciones.forEach(e => {
      if (last_departament != null) {
        if (e.nombreDepartamentp !== last_departament.nombreDepartamentp) {
          last_departament = e;
          this.departamentos.push(e);
        }
      } else {
        last_departament = e;
        this.departamentos.push(e);
      }
    });
    this.dataSource = new MatTableDataSource<any>(this.departamentos);
    this.dataSource.sort = this.sort;
  }

  public setCheckBox(e: any, item: any) {
    if (e.checked === true) {
      this.departamentoSelected = item;
      this.selection.clear();
      this.selection.select(item);
      this.selected = true;
      this.datos.patchValue({
        divipo: this.departamentoSelected.idGeDivipo,
        codigo: this.departamentoSelected.codigoDepartemento,
        nombre: this.departamentoSelected.nombreDepartamentp,
      });
    } else {
      this.departamentoSelected = null;
      this.selected = false;
      this.selection.clear();
      this.datos.patchValue({
        divipo: '',
        codigo: '',
        nombre: '',
      });
    }
  }

  public close(tipo: number): void {
    this.dialogRef.close({tipo: tipo});
  }

  public select(): void {
    this.dialogRef.close({tipo: 1, item: this.departamentoSelected});
  }

}
