import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowErrorsDirective } from './show-errors.component';

describe('ShowErrorsComponent', () => {
    let component: ShowErrorsDirective;
    let fixture: ComponentFixture<ShowErrorsDirective>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ShowErrorsDirective]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ShowErrorsDirective);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});