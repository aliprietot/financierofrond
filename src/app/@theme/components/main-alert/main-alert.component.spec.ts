import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainAlertComponent } from './main-alert.component';

describe('MainAlertComponent', () => {
  let component: MainAlertComponent;
  let fixture: ComponentFixture<MainAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
