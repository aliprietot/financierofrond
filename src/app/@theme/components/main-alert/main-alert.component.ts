import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-main-alert',
  templateUrl: './main-alert.component.html',
  styleUrls: ['./main-alert.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MainAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // SOCIAL PANEL JS
    const floating_btn = document.querySelector('.floating-btn');
    const close_btn = document.querySelector('.close-btn');
    const social_panel_container = document.querySelector('.social-panel-container');

    floating_btn.addEventListener('click', () => {
      social_panel_container.classList.toggle('visible');
    });

    close_btn.addEventListener('click', () => {
      social_panel_container.classList.remove('visible');
    });
  }

}
