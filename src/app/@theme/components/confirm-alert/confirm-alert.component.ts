import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-alert',
  templateUrl: './confirm-alert.component.html',
  styleUrls: ['./confirm-alert.component.scss'],
})
export class ConfirmAlertComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ConfirmAlertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    // SOCIAL PANEL JS
    const floating_btn = document.querySelector('.floating-btn');
    const close_btn = document.querySelector('.close-btn');
    const social_panel_container = document.querySelector('.social-panel-container');

/*     floating_btn.addEventListener('click', () => {
      social_panel_container.classList.toggle('visible');
    });

    close_btn.addEventListener('click', () => {
      social_panel_container.classList.remove('visible');
    }); */
  }

  public confirmar(): void {
    this.dialogRef.close(1);
  }

  public close(): void {
    this.dialogRef.close(0);
  }

}
