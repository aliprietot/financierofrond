import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulosMenuComponent } from './modulos-menu.component';

describe('ModulosMenuComponent', () => {
  let component: ModulosMenuComponent;
  let fixture: ComponentFixture<ModulosMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModulosMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulosMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
