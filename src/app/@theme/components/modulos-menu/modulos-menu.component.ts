import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../../../settings/services/app.service';
import { AuthService } from '../../../settings/services/auth.service';

@Component({
  selector: 'app-modulos-menu',
  templateUrl: './modulos-menu.component.html',
  styleUrls: ['./modulos-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModulosMenuComponent implements OnInit {

  public userName = '';
  public userImage = 'assets/img/user-default.png';

  public modulos: Array<any> = [
    { name: 'Predial', icon: 'account_balance', ruta: 'pages/menu-predial' },
    { name: 'ICA', icon: 'library_books', ruta: 'pages/ica' },
    { name: 'Configuracion', icon: 'settings', ruta: 'pages/config/menu-config' },
    { name: 'Recaudos', icon: 'description', ruta: 'pages/reportes' },
    { name: 'Estadisticas', icon: 'dashboard', ruta: 'pages/predial/estadisticas' },
  ];

  constructor(
    private _ApiRestService: AppService,
    private _AuthService: AuthService,
  ) { }

  ngOnInit() {
    this.userName = String(this._AuthService.getUsuarioLogueado(sessionStorage.getItem('token')).user_name);
  }

  public getModulo(index: number): void {
    this._ApiRestService.goTo(this.modulos[index].ruta);
  }

}
