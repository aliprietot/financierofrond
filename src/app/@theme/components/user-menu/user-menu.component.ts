import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PasswordComponent } from '../password/password.component';
import { MatDialog } from '@angular/material/dialog';
import { AlertService } from '../../../settings/services/alert.service';
import { AuthService } from '../../../settings/services/auth.service';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class UserMenuComponent implements OnInit {

  public userName = '';
  public userImage = 'assets/img/user-default.png';

  constructor(
    private _AuthService: AuthService,
    private _AlertService: AlertService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.userName = String(this._AuthService.getUsuarioLogueado(sessionStorage.getItem('token')).user_name);
  }

  public openFormPassword() {
    const dialogRef = this.dialog.open(PasswordComponent, {
      width: '35%', height: 'auto', disableClose: true, backdropClass: 'dark', panelClass: 'panel',
    });
    dialogRef.afterClosed().subscribe(result => {});
  }

  public logOut() {
    this._AlertService.confirmAlert('Seguro que desea cerrar Sesion?')
    .then((e: any) => {if (e === true ) {this._AuthService.logout(); }});
  }

}
