import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSidebarModule,
  NbUserModule,
  NbContextMenuModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbThemeModule,
  NbInputModule,
  NbCardModule,
  NbDatepickerModule,
  NbTabsetModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NbSecurityModule } from '@nebular/security';
import { FlexLayoutModule } from '@angular/flex-layout';

import {
  FooterComponent,
  HeaderComponent,
  SearchInputComponent,
  PasswordComponent,
  GdiviposComponent,
  BreadcrumbComponent,
  ConfirmAlertComponent,
  UserMenuComponent,
  ModulosMenuComponent,
  MainAlertComponent,
  ShowErrorsDirective,
} from './components';

import {
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  TruncatePipe,
} from './pipes';
import {
  OneColumnLayoutComponent,
  ThreeColumnsLayoutComponent,
  TwoColumnsLayoutComponent,
} from './layouts';
import { DEFAULT_THEME } from './styles/theme.default';
import { COSMIC_THEME } from './styles/theme.cosmic';
import { CORPORATE_THEME } from './styles/theme.corporate';
import { DARK_THEME } from './styles/theme.dark';

import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule, ReactiveFormConfig } from '@rxweb/reactive-form-validators';
ReactiveFormConfig.set({'validationMessage': {'compare': 'Las contraseñas no Concuerdan'}});
import {SidebarModule} from 'primeng/sidebar';

const NB_MODULES = [
  NbLayoutModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbContextMenuModule,
  NbSecurityModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbEvaIconsModule,
  NbInputModule,
  NbDatepickerModule,
  MatCardModule,
  MatIconModule,
  NbCardModule,
  NbTabsetModule,
];
const COMPONENTS = [
  GdiviposComponent,
  HeaderComponent,
  FooterComponent,
  SearchInputComponent,
  OneColumnLayoutComponent,
  PasswordComponent,
  ThreeColumnsLayoutComponent,
  TwoColumnsLayoutComponent,
  BreadcrumbComponent,
  ConfirmAlertComponent,
  UserMenuComponent,
  ModulosMenuComponent,
  MainAlertComponent,
  ShowErrorsDirective,
];
const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe,
  TruncatePipe,
];

const ENTRYCOMPONENTS = [
  PasswordComponent,
  GdiviposComponent,
  ConfirmAlertComponent,
  MainAlertComponent
];

@NgModule({
  imports: [
    CommonModule,
    ...NB_MODULES,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    MatTableModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatGridListModule,
    FlexLayoutModule,
    SidebarModule,
  ],
  exports: [ CommonModule, ...PIPES, ...COMPONENTS ],
  declarations: [ ...COMPONENTS, ...PIPES ],
  entryComponents: [ ...ENTRYCOMPONENTS ],
})

export class ThemeModule {
  static forRoot(): ModuleWithProviders<ThemeModule> {
    return <ModuleWithProviders<ThemeModule>>{
      ngModule: ThemeModule,
      providers: [
        ...NbThemeModule.forRoot(
          { name: 'default' },
          [ DEFAULT_THEME, COSMIC_THEME, CORPORATE_THEME, DARK_THEME ],
        ).providers,
      ],
    };
  }
}
