import { TestBed, async, inject } from '@angular/core/testing';

import { RolGuard } from './Rol.guard';

describe('RolGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RolGuard]
    });
  });

  it('should ...', inject([RolGuard], (guard: RolGuard) => {
    expect(guard).toBeTruthy();
  }));
});

