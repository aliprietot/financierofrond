import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'app/settings/services/auth.service';
import { AlertService } from '../services/alert.service';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root',
})

export class PredialGuard implements CanActivate {

  public usuario: any;
  public valid = true;
  public codigo_valid = true;

  constructor(
    private _Router: Router,
    private _Location: Location,
    private _AuthService: AuthService,
    private _AlertService: AlertService,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      this.usuario = this._AuthService.getUsuarioLogueado(sessionStorage.getItem('token'));
      for ( let index = 0; index < this.usuario.tributos.length; index++ ) {
        const item = this.usuario.tributos[index];
        if ( item.codigo === '001' ) {
          this.valid = true;
          this.codigo_valid = true;
          break;
        } else {
          this.valid = false;
          this.codigo_valid = false;
        }
      }

      if ( !this.codigo_valid ) {
        this._AlertService.showWarning('no tiene acceso a predial!');
        this._Location.back();
        this.valid = false;
      }

    return this.valid;
  }
}
