import { TestBed, async, inject } from '@angular/core/testing';

import { EstampillasGuard } from './estampillas.guard';

describe('EstampillasGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EstampillasGuard]
    });
  });

  it('should ...', inject([EstampillasGuard], (guard: EstampillasGuard) => {
    expect(guard).toBeTruthy();
  }));
});
