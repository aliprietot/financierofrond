import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { AlertService } from '../services/alert.service';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root',
})

export class RolGuard implements CanActivate {

  public usuario: any;
  public token: any;
  public valid = true;

  constructor(
    private _Location: Location,
    private _AlertService: AlertService,
    private auth: AuthService,
    private router: Router) {
    this.usuario = this.auth.obtenerDatosToken(sessionStorage.getItem('token'));
  }

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if ( this.usuario.entidad.idEntidad === 0) {
      return true;
    } else {
      this._AlertService.showWarning('no puedes ingresar!');
      this._Location.back();
    }
  }
}
