import { Injectable, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate, OnInit {

  public usuario: any;
  public token: any;
  public valid = false;

  constructor(
    private _JwtHelperService: JwtHelperService,
    private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.token = sessionStorage.getItem('token');
  }

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.auth.isAuthenticated()) {
      this.valid = true;
      if ( this.auth.isTokenExpired() ) {
        this.valid = true;
      } else {
        this.valid = false;
      }
    } else {
      this.valid = false;
    }

    if (this.valid === true) {
      return true;
    } else {
      this.router.navigate(['auth/login']);
      window.location.reload();
      return false;
    }
  }
}
