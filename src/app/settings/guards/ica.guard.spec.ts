import { TestBed, async, inject } from '@angular/core/testing';

import { IcaGuard } from './ica.guard';

describe('IcaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IcaGuard]
    });
  });

  it('should ...', inject([IcaGuard], (guard: IcaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
