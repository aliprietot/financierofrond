import { Injectable, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { ApiRestService } from '../services/api-rest.service';

@Injectable({
  providedIn: 'root'
})

export class LoginGuard implements CanActivate, OnInit {

  public usuario: any;
  public token: any;
  public valid = true;

  constructor(private _ApiRestService: ApiRestService, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.token = sessionStorage.getItem('token');
  }

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.auth.isAuthenticated()) {
      if (this.auth.isTokenExpired()) { this.valid = true; }
      this.valid = true;
    }
    if (this.valid === true) {
      this.router.navigate(['pages/escritorio']);
      return true;
    } else {
      return false;
    }
  }
}
