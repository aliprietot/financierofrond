import { TestBed, async, inject } from '@angular/core/testing';

import { PredialGuard } from './predial.guard';

describe('PredialGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PredialGuard]
    });
  });

  it('should ...', inject([PredialGuard], (guard: PredialGuard) => {
    expect(guard).toBeTruthy();
  }));
});
