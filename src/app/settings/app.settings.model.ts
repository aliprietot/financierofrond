export class Settings {
    constructor(
        public loadingtext: string,
        public navbartext: string,
        public endpoints: any,
        public tributos_activos: Array<any>,
        public messages: Array<any>,
    ) { }
}
