export const APP: any = {
  //   urlBack: 'http://localhost:5000/',
     urlBack: 'http://181.58.120.41:8090/',
     urlFront: 'http://localhost:4200/',
 };


export class Factura {
  constructor(
    public cod_fac: number,
    public ref: string,
    public CC: string,
    public nombre: string,
    public periodos: string,
    public saldo_capital: number,
    public saldo_intereses: number,
    public descuentos: number,
    public neto: number,
  ) { }
}

class Menu {
  constructor(  
    public nombre: string,
    public descripcion: string,
    public ruta: string,
    public estado: boolean,
    public icon: string,
  ) { }
  
} 

export const menu_principal: Menu[] = [
  new Menu('CONTABILIDAD', 'Formularios de procesos tributarios', 'pages/menu-contabilidad', false, 'file-invoice-dollar'),
  new Menu('PRESUPUESTO', 'Formularios de procesos tributarios', 'pages/menu-presupuesto', true, 'file-invoice-dollar'),
  new Menu('TESORERIA', 'Formularios de procesos tributarios', 'pages/menu-tesoreria', false, 'file-invoice-dollar'),
  new Menu('NOMINA', 'Formularios de procesos tributarios', 'pages/menu-nomina', false, 'file-invoice-dollar'),

];

export const menu_presupuesto: Menu[] = [
  new Menu('Configuraciones', '', 'pages/presupuesto/configuraciones-presupuesto', true, 'cogs'),
  new Menu('Certificado de disponibilidad presupuestal ', '', 'pages/presupuesto/certificado', true, 'money-check'),
  new Menu('Presupuesto público', '', 'pages/presupuesto/presupuesto-publico', true, 'money-check'),
  new Menu('Interfases y Registros', '', 'pages/presupuesto/interfases', true, 'money-check'),
  new Menu('Ciclo Presupuestal de contratación', '', 'pages/presupuesto/ciclos', true, 'money-check'),
];

export const menu_configuraciones: Menu[] = [
  new Menu('Homologaciones', '', 'pages/presupuesto/configuraciones-presupuesto/homologacion', false, 'money-check'),
  new Menu('centros de costos', '', 'pages/presupuesto/configuraciones-presupuesto/centros-costos',false ,'money-check'),
  new Menu('creación de terceros', '', 'pages/presupuesto/configuraciones-presupuesto/terceros',true ,'money-check'),
  new Menu('configuraciones presupuestales', '', 'pages/presupuesto/configuraciones-presupuesto/configuracion',false ,'money-check'),

];

export const menu_presupuesto_publico: Menu[] = [
  new Menu('Presupuesto de Gastos', '', 'page/presupuesto/presupuesto-publico/gastos', false, 'money-check'),
  new Menu('Presupuesto de Ingresos ', '', 'page/presupuesto/presupuesto-publico/ingresos',false ,'money-check'),
  new Menu('Movimientos Presupuestales', '', 'page/presupuesto/presupuesto-publico/moviminetos', false, 'money-check')

];




