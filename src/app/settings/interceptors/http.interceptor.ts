import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse,
    HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { AppService } from '../services/app.service';
import { AlertService } from '../services/alert.service';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

    constructor(
        public toasterService: ToastrService,
        private _ApiRestService: AppService,
        private _AlertService: AlertService,
        ) { }


    public intercept(
        req: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {

        this._AlertService.openSpinner();

        return next.handle(req).pipe(
            tap(evt => {
                if (evt instanceof HttpResponse) {
/*                     if (evt.body && evt.body.success) {
                          this.toasterService.success
                            (evt.body.success.message, evt.body.success.title, { positionClass: 'toast-bottom-center' })
                            this._ApiRestService.showSuccess('Peticion Realizada');
                            this._ApiRestService.closeSpinner();
                    } */
                    this._AlertService.closeSpinner();
                }
            }),
            catchError((error: any) => {
                if (error.error instanceof ErrorEvent) {
                    this._AlertService.showError('Error al consultar');
                    this._AlertService.closeSpinner();
                } else {
                    switch (error.status) {
                        case 401:      // login
                            this._AlertService.showError('NO AUTORIZADO!');
                            this._ApiRestService.goTo('/auth');
                            break;
                        case 403:
                            this._AlertService.showError('PROHIBIDO!');
                            break;
                        case 404:
                            this._AlertService.showError('RECURSO NO ENCONTRADO!');
                            break;
                        case 405:
                            this._AlertService.showError('NO PERMITIDO!');
                            break;
                        case 408:
                            this._AlertService.showError('SE AGOTO EL TIEMPO DE ESPERA!');
                            break;
                        case 415:
                            this._AlertService.showError('FORMATO NO SOPORTADO!');
                            break;
                        case 500:
                            this._AlertService.showError('ERROR EN EL SERVIDOR!');
                            break;
                        case 501:
                            this._AlertService.showError('CONSULTA NO SOPORTADA!');
                            break;
                    }
                    this._AlertService.closeSpinner();
                }
                return of(error);
            }));
    }
}
