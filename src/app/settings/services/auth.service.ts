import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppSettings } from '../app.settings';
import { Settings } from '../app.settings.model';
import { APP } from '../const';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  public _usuario: Usuario;
  public _token: string;
  public url = APP.urlBack;
  private httpOptions;
  private httpMultipartOption;
  public settings: Settings;

  constructor(
    private http: HttpClient, 
    private router: Router,
    public appSettings: AppSettings) {
      this.settings = this.appSettings.settings;
    }

  public get usuario(): Usuario {
    if ( this._usuario !== null ) {
      return this._usuario;
    } else if ( this._usuario === null && sessionStorage.getItem('usuario') !== null) {
      this._usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
      return this._usuario;
    }
    return new Usuario();
  }

  public get token(): string {
    if ( this._token != null ) {
      return this._token;
    } else if ( this._token == null && sessionStorage.getItem('token') != null) {
      this._token = sessionStorage.getItem('token');
      return this._token;
    }
  }

  public getUsuarioLogueado(access_token: string) {
    const payload = this.obtenerDatosToken(access_token);
    return payload;
  }

  isTokenExpired(): boolean {
    let token: string = sessionStorage.getItem('token');
    let payload = this.obtenerDatosToken(token);
    let now = new Date().getTime() / 1000;
    if (payload.exp < now) {
      return true;
    }
    return false;
  }
  

  setToken( access_token: string): void {
    this._token = access_token;
    sessionStorage.setItem('token',access_token);
  }


  obtenerDatosToken( access_token: string): any {
    if (access_token != null || access_token != undefined || access_token != '') {
      return JSON.parse(atob(access_token.split('.')[1]));
    }
    else{
      sessionStorage.clear();
      this.router.navigate(['login']);
    }
  }

  isAuthenticated(): boolean {
    let token = sessionStorage.getItem('token');
    let payload = this.obtenerDatosToken(token);
    if (payload != null && payload.user_name && payload.user_name.length > 0) {
      return true;
    }
    return false;
  }

  hasRole(role: any): boolean {
    if (this.usuario != null && this.usuario.roles != null && this.usuario.roles.includes(role)) {
      return true;
    }
    return false;
  }

  logout(): void {
    this._token = null;
    this._usuario = null;
    sessionStorage.clear();
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('usuario');
    window.location.replace(this.url+'#/login');
  }

  obtenerDatosUser() {
    if ( this._usuario !== null ) {
      return this._usuario;
    } else if ( this._usuario === null && sessionStorage.getItem('usuario') !== null) {
      this._usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
      return this._usuario;
    }
    return new Usuario();
  }

  login(data: any): Observable<any> {
    const credenciales = btoa('angularapp' + ':' + '12345');
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + credenciales,
    });
    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', data.username);
    params.set('password', data.password);
    return this.http.post<any>(this.url+'oauth/token', params.toString(), { headers: httpHeaders });
  }
}