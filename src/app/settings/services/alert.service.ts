import { Injectable, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Settings } from '../app.settings.model';
import {
  SnotifyService,
  SnotifyPosition,
  SnotifyToastConfig,
} from 'ng-snotify';
import { AppSettings } from '../app.settings';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmAlertComponent } from '../../@theme/components/confirm-alert/confirm-alert.component';

import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE =
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class AlertService implements OnInit {

  private destroy$: Subject<void> = new Subject<void>();
  public settings: Settings;
  public style = 'dark';
  public timeout = 3000;
  public position: SnotifyPosition = SnotifyPosition.rightTop;
  public progressBar = true;
  public closeClick = true;
  public newTop = true;
  public filterDuplicates = false;
  public backdrop = -1;
  public dockMax = 8;
  public blockMax = 6;
  public pauseHover = true;
  public titleMaxLength = 15;
  public bodyMaxLength = 80;

  constructor(
    private dialog: MatDialog,
    public appSettings: AppSettings,
    private _NgxSpinnerService: NgxSpinnerService,
    private _SnotifyService: SnotifyService,
  ) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {}

  getConfig(): SnotifyToastConfig {
    this._SnotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
        // filterDuplicates: this.filterDuplicates
      },
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: this.backdrop,
      position: this.position,
      timeout: this.timeout,
      showProgressBar: this.progressBar,
      closeOnClick: this.closeClick,
      pauseOnHover: this.pauseHover,
    };
  }

  public openSpinner() {
    this._NgxSpinnerService.show();
  }

  public closeSpinner() {
    this._NgxSpinnerService.hide();
  }

  public changeNavBarText(text: string) {
    this.settings.navbartext = text;
  }

  public changeLoadingText(text: string) {
    this.settings.loadingtext = text;
  }

  public showSuccess(body: string) {
    const title = 'Exito!';
    this._SnotifyService.success(body, title, this.getConfig());
  }

  public showInfo(body: string) {
    const title = 'Informacion!';
    this._SnotifyService.info(body, title, this.getConfig());
  }

  public showWarning(body: string) {
    const title = 'Advertencia!';
    this._SnotifyService.warning(body, title, this.getConfig());
  }

  public showError(body: string) {
    const title = 'Error!';
    this._SnotifyService.error(body, title, this.getConfig());
  }

  public showAdminAlert(body: string) {
    const href = String(window.location.href);
    const url = href.split('#')[0];
    const icon = `${url}assets/img/logo2.png`;
    this._SnotifyService.simple(body, {
      ...this.getConfig(),
      icon: icon,
    });
  }

  public async confirmAlert(text: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const dialogRef = this.dialog.open(ConfirmAlertComponent, {
        data: text,
        width: '40%',
        height: 'auto',
        maxHeight: '80%',
        disableClose: true,
        backdropClass: 'dark',
        panelClass: 'panel',
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result === 1) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  public exportExcel(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ['data'],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: 'xlsx',
      type: 'array',
    });
    this.saveAsExcelFile(excelBuffer, excelFileName);
    this.closeSpinner();
  }

  public saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(
      data,
      fileName + '_EXPORT_' + new Date().getTime() + EXCEL_EXTENSION,
    );
  }

  public _Destroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
