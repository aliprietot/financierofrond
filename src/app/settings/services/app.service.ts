import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpEvent } from '@angular/common/http';

import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import Swal from 'sweetalert2';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
} from '@nebular/theme';


@Injectable({providedIn: 'root'})

export class AppService {

  private httpHeaders = new HttpHeaders({ 'Content-type': 'application/json' });
  private httpHeaders2 = new HttpHeaders('multipart/*');
  public index = 1;
  public destroyByClick = true;
  public duration = 2000;
  public hasIcon = true;
  public position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  public preventDuplicates = false;
  public types: NbComponentStatus[] = ['primary', 'success', 'info', 'warning', 'danger'];
  public configAlert = {
    status: this.types[1],
    destroyByClick: this.destroyByClick,
    duration: this.duration,
    hasIcon: this.hasIcon,
    position: this.position,
    preventDuplicates: this.preventDuplicates,
  };

  private httpOptions;
  private token = '';
  public url = '';

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private router: Router,
    private auth: AuthService,
    private toastrService: NbToastrService,
    private spinner: NgxSpinnerService) {
      this.getUrl();
  }


  public async getUrl() {
    const urlserver: any = await this.getEndPoints();
    this.url = urlserver.endPoint;
  }

  private getHeaders() {
    this.token = sessionStorage.getItem('token');

    if (this.token == null || this.token == undefined || this.token == '') {
      this.router.navigate(['/login']);
    } else {
      this.httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token }) };
    }
  }

  public async get(ruta: string): Promise<any> {
    return await this.http.get<any>(`${this.url}${ruta}`, {headers: this.agregarAuthorizationHeader()}).toPromise();
  }

  public post(ruta: string, body: any) {
    return this.http.post(`${this.url}${ruta}`, body, { headers: this.agregarAuthorizationHeader() });
  }

  public delete(ruta: string) {
    return this.http.delete(`${this.url}${ruta}`, { headers: this.agregarAuthorizationHeader() });
  }

  public put(ruta: string, body: any) {
    return this.http.put(`${this.url}${ruta}`, body, { headers: this.agregarAuthorizationHeader() });
  }


  public agregarAuthorizationHeader() {
    const token = sessionStorage.getItem('token');
    if ((token != null) && (token !== '') && (token !== undefined)) {
      return this.httpHeaders.append('Authorization', 'Bearer' + token);
    } else {
      sessionStorage.clear();
      this.goTo('auth');
    }
    return this.httpHeaders;
  }

  clearSession(): void {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }

  public openSpinner() {
    this.spinner.show();
  }

  public closeSpinner() {
    this.spinner.hide();
  }

  public showSuccess(text: string) {
    this.toastr.success(text, 'Exito!' );
  }

  public showInfo(text: string) {
    this.toastr.info(text, 'Informacion!' );
  }

  public showWarning(text: string) {
    this.toastr.warning(text, 'Alerta!');
  }

  public showError(text: string) {
    this.toastr.error(text, 'Error!');
  }

  public showWait() {
    this.spinner.show();
  }

  public hideWait() {
    this.spinner.hide();
  }

  public goTo(ruta: string) {
    this.router.navigate([ruta]);
  }

  public getDataUser() {
    const token = sessionStorage.getItem('token');
    const payload = JSON.parse(atob(token.split('.')[1]));
    const usuario: any = {};
    usuario.id = payload.id;
    usuario.nombre = payload.nombre;
    usuario.apellido = payload.apellido;
    usuario.email = payload.email;
    usuario.empresa = payload.empresa;
    usuario.username = payload.user_name;
    usuario.roles = payload.authorities;
    return usuario;
  }

  public setItemStore(name: string, data: any) {
    sessionStorage.setItem(name, data);
  }

  public getItemStore(name: string) {
    return sessionStorage.getItem(name);
  }

  public go(ruta: string) {
    this.router.navigate([ruta]);
  }



  public confirmAlert(text: string, textConfirm: string): Promise<boolean> {
   return new Promise((resolve, reject) => {
      Swal.fire({
        text: text,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: textConfirm,
      }).then((result) => {
        result.value ? (resolve(true)) :
        (result.dismiss === Swal.DismissReason.cancel ?
        ( resolve(false)) : resolve(false));
      });
    });
  } 

  public async getEndPoints(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const url = String(window.location.href).split('#')[0];
      this.http.get(`${url}assets/endpoints/endpoint.json`)
      .subscribe((data: any) => {resolve(data); });
    });
  }

}
