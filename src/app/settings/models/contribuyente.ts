export class Contribuyente {

    public uso: any;
    public tipo: any;
    public destino_economico: any;
    public estrato: any;

    constructor(
        public codigo: string,
        public propietario: string,
        public direccion: string,
        public documento: string,
        public area_terreno: string,
        public area_edificada: string,
        public vigencia: string,
        public ultimo_avaluo: number,
        public matricula: string,
        public celular: string,
        public email: string,
        public estado: string,
    ) {
        this.uso = { usoNom: '' };
        this.tipo = '';
        this.destino_economico = '';
        this.estrato = '';
    }
}
