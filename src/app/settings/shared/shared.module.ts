import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '../pipes/pipes.module';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxMaskModule } from 'ngx-mask';
import { RxReactiveFormsModule, ReactiveFormConfig } from '@rxweb/reactive-form-validators';
ReactiveFormConfig.set({'validationMessage': {'compare': 'Las contraseñas no Concuerdan'}});

import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { FlexLayoutModule } from '@angular/flex-layout';
import {ChartModule} from 'primeng/chart';

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbTooltipModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbFormFieldModule,
} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';

import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatRadioModule } from '@angular/material/radio';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatChipsModule } from '@angular/material/chips';
import { MatSelectModule } from '@angular/material/select';

const MODULES = [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    NbActionsModule,
    NbButtonModule,
    NbTooltipModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbFormFieldModule,
    ThemeModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    PerfectScrollbarModule,
    NgxSkeletonLoaderModule,
    RxReactiveFormsModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSidenavModule,
    MatChipsModule,
    MatRadioModule,
    MatSelectModule,
    MatButtonModule,
    MatExpansionModule,
    ChartModule,
    FlexLayoutModule,
];

const SERVICES = [
  NgxSpinnerService,
];
const COMPONENTS = [];
const ENTRY_COMPONENTS = [];

@NgModule({
  declarations: [],
  providers: [
    ...SERVICES,
  ],
  imports: [
    NgxMaskModule.forRoot(),
    ToastrModule.forRoot({ preventDuplicates: true}),
    ...MODULES,
  ],
  exports: [
    NgxMaskModule,
    ...MODULES,
    ToastrModule,
  ],
})
export class SharedModule { }
