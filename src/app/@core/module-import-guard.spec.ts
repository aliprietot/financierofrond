import { TestBed, async, inject } from '@angular/core/testing';

import { ModuleImportGuardGuard } from './module-import-guard.guard';

describe('ModuleImportGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModuleImportGuardGuard]
    });
  });

  it('should ...', inject([ModuleImportGuardGuard], (guard: ModuleImportGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});

