import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { SeoService } from './@core/utils/seo.service';
import { Settings } from './settings/app.settings.model';
import { AppSettings } from './settings/app.settings';
import { Event, Router, NavigationStart, NavigationEnd, RouterEvent } from '@angular/router';
import { PlatformLocation } from '@angular/common';
import { AppService } from './settings/services/app.service';

@Component({
  selector: 'ngx-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements OnInit {

  public showLoading = true;
  public settings: Settings;
  public currentUrl = '';

  constructor(
    private _ApiRestService: AppService,
    public appSettings: AppSettings,
    private location: PlatformLocation,
    private router: Router,
    private analytics: AnalyticsService,
    private seoService: SeoService) {
      this.settings = this.appSettings.settings;

      this.router.events.subscribe((routerEvent: Event) => {
        if (routerEvent instanceof NavigationStart) {
          this.showLoading = true;
          this.currentUrl = routerEvent.url.substring(routerEvent.url.lastIndexOf('/') + 1);
        }

        if (routerEvent instanceof NavigationEnd) {
          this.showLoading = false;
        }
        window.scrollTo(0, 0);
      });
  }

  async ngOnInit() {
    this.analytics.trackPageViews();
    this.seoService.trackCanonicalChanges();
    // setInterval(() => {console.clear(); }, 3000);
  }

}

